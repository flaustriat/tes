# Documents pour les élèves de TES de M. Laustriat

## Cours intégration

* [Cours intégration](7-integration/cours-integration.pdf)
* [Exercices de type bac avec du calcul intégral](7-integration/ex-bac-integration.pdf)

## Cours lois à densité

 * [Cours lois à densité](8-lois_densite/cours-loisDensite.pdf)
 * [Exercices bac lois à densité](8-lois_densite/ex-bac-loi-densite.pdf)
 * [Exercice bac lois à densité](8-lois_densite/ex-bac-loi-densite2.pdf)
 
## Cours échantillonages et estimation
 * [Cours estimation](9_estimation/cours-estimation.pdf) 



# Trace écrite des cours en classe virtuelle.
Vous trouverez la trace écrite des cours [ici](traceEcrite). Le nom du fichier correspond à la date au format mois jour.

# Objectif préparation du bac:

Vous pouvez faire des exercices de type bac sur les probabilités conditionnelles, loi binomiale et suites sur les fonctions hormis si il y a une partie sur l'intégration vous devriez pouvoir tout faire.

Vous trouverez tous les sujets et corrigés sur le site de l'APMEP:  https://www.apmep.fr/TES-Annee-2018-1-sujet
Dans ce fichier vous avez un index par notion à la fin et donc de trouver directement un exercice sur le sujet que vous souhaitez travailler.

https://www.apmep.fr/IMG/pdf/ES_Annee_2018_2.pdf
